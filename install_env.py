import os

# create a new conda environement
# conda create -n dpl4life_nlp python=3.8

list_packages = [
    "conda install --yes pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch",
    "conda install --yes -c conda-forge tensorboard",
    "conda install --yes -c conda-forge pytorch-lightning",
    "conda install --yes -c pytorch torchtext",
    "conda install --yes -c conda-forge spacy",
    "conda install --yes -c conda-forge jupyterlab"

]

for package in list_packages:
    os.system(package)