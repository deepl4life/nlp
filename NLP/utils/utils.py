import unicodedata
import string
from typing import List
from pathlib import Path


def unicode_to_ascii(s: str, all_letters: str = None) -> str:
    """ Transliterate any unicode string into the closest possible representation in ascii text
    code find here: https://stackoverflow.com/a/518232/2809427

    :param s: the unicode string to transform
    :param all_letters: string containing all the authorized characters
    :return: the ascii string
    """
    if all_letters is None:
        all_letters = string.ascii_letters + " .,;'"

    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in all_letters)


def read_lines(filename: Path) -> List[str]:
    """ Read a file and split into lines

    :param filename: path to the file
    :return: a list of the lines contain in the files transliterate into ascii
    """
    if filename.suffix != ".txt":
        raise ValueError(f"the filename is not a .txt file but a {filename.suffix}")

    with open(filename, encoding='utf-8') as file:
        lines = file.read().strip().split('\n')
    return [unicode_to_ascii(line) for line in lines]