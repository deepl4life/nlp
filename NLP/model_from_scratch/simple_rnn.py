import torch
import torch.nn as nn

"""
DISCLAIMER: This is a simple implementation of a Recurrent Neural Network. This code is a way to learn how a RNN work.
If you want to use a RNN for a real application, I advise you to use the pytorch implementation.
"""


class SimpleRNN(nn.Module):

    def __init__(self, input_size, hidden_size, output_size):
        super(SimpleRNN, self).__init__()
        self.hidden_size = hidden_size
        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def init_hidden(self):
        return torch.zeros(1, self.hidden_size)

    def forward(self, input, hidden):
        combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(combined)
        output = self.i2o(combined)
        output = self.softmax(output)

        return output, hidden
