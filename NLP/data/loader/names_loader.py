import torch
from typing import Optional, List
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import LightningDataModule
from NLP.data.dataset.names_dataset import NamesDataset


def _collate_fn(batch: List[torch.Tensor]) -> tuple:
    return tuple(zip(*batch))


class NamesDataLoader(LightningDataModule):

    def __init__(self,
                 path_dataset: str = None,
                 num_workers: int = 0,
                 batch_size: int = 2):
        super(NamesDataLoader, self).__init__()
        self.path_dataset = path_dataset
        self.num_workers = num_workers
        self.batch_size = batch_size

    def setup(self, stage: Optional[str] = None) -> None:
        if stage == 'fit' or stage is None:
            self.train_dataset = NamesDataset(path_dataset=self.path_dataset)
            self.num_letters = self.train_dataset.num_letters
            self.num_categories = len(self.train_dataset.categories)

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.train_dataset, shuffle=True)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=False,
                          pin_memory=True,
                          collate_fn=_collate_fn)