import string
import random
import torch
from torch.utils.data.dataset import Dataset
from pathlib import Path
from NLP.utils.utils import read_lines


def random_list_choice(l: list):
    """ Return a random value from a list """
    return l[random.randint(0, len(l) - 1)]


class NamesDataset(Dataset):
    """ Load the data from the names dataset """

    def __init__(self, path_dataset: str):
        self.path_dataset = Path(path_dataset)
        self.all_letters = string.ascii_letters + " .,;'"
        self.num_letters = len(self.all_letters)
        self.category_lines = {}
        self.categories = []
        self._load_data()

    def _load_data(self):
        """ Fill categories and category_lines """
        for filename in self.path_dataset.glob("*.txt"):
            category = filename.stem
            self.categories.append(category)
            lines = read_lines(filename)
            self.category_lines[category] = lines

    def _letter_to_index(self, letter) -> int:
        """ Find letter index from all_letters, e.g 'a' = 0 """
        return self.all_letters.find(letter)

    def _line_to_tensor(self, line) -> torch.Tensor:
        """ Turn a line into a <line_length x 1 x n_letters>, i.e an array of one-hot letter vectors """
        tensor = torch.zeros(len(line), 1, self.num_letters)
        for index_letter, letter in enumerate(line):
            tensor[index_letter][0][self._letter_to_index(letter)] = 1
        return tensor

    def __len__(self) -> int:
        return sum([len(names) for key, names in self.category_lines.items()])

    def __getitem__(self, item):
        """ Select a random name from the dataset and return its category and tensor """
        category = random_list_choice(self.categories)
        line = random_list_choice(self.category_lines[category])
        category_tensor = torch.tensor([self.categories.index(category)])
        line_tensor = self._line_to_tensor(line)
        return category, line, category_tensor, line_tensor
