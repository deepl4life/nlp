import torch
from pytorch_lightning import LightningModule, LightningDataModule
from NLP.model_from_scratch.simple_rnn import SimpleRNN


class NamesAnalyser(LightningModule):

    def __init__(self,
                 datamodule: LightningDataModule,
                 simple_rnn: bool = True,
                 learning_rate: float = 0.005,
                 nb_steps: int = 50000):
        super().__init__()
        self.datamodule = datamodule
        self.learning_rate = learning_rate
        self.nb_steps = nb_steps

        if simple_rnn:
            self.model = self._build_simple_rnn(n_hidden=128)

    def _build_simple_rnn(self, n_hidden) -> torch.nn.Module:
        n_letters = self.datamodule.num_letters
        n_categories = self.datamodule.num_categories
        return SimpleRNN(n_letters, n_hidden, n_categories)

    def _train_simple_rnn(self, category_tensor, line_tensor):

        if self.datamodule.batch_size != 1:
            raise ValueError("SimpleRNN works only with batch_size =1 !")

        category_tensor, line_tensor = category_tensor[0], line_tensor[0]
        hidden = self.model.init_hidden()

        for i in range(line_tensor.size()[0]):
            output, hidden = self.model(line_tensor[i], hidden)

        criterion = torch.nn.NLLLoss()

        return criterion(output, category_tensor)

    def training_step(self, batch, batch_idx):

        category, line, category_tensor, line_tensor = batch
        if isinstance(self.model, SimpleRNN):
            loss = self._train_simple_rnn(category_tensor, line_tensor)

        self.log('loss', loss)

        return loss

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = torch.optim.SGD(params=trainable_parameters,
                                    lr=self.learning_rate,
                                    momentum=0.9,
                                    weight_decay=0.005)

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer,
                                                                  T_max=self.nb_steps)

        return [optimizer], [lr_scheduler]
