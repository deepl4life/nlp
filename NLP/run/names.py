from NLP.data.loader.names_loader import NamesDataLoader
from NLP.models.names_analyser import NamesAnalyser
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning import Trainer


def main():
    datamodule = NamesDataLoader(path_dataset=r"D:\PycharmProjects\dataset\nlp\names_dataset",
                                 num_workers=0,
                                 batch_size=1)

    datamodule.setup()
    name_analyser = NamesAnalyser(datamodule=datamodule,
                                  simple_rnn=True,
                                  learning_rate=0.005,
                                  nb_steps=5000)

    logger = TensorBoardLogger(save_dir="./lightning_logs/",
                               name="names_test",
                               default_hp_metric=False)

    trainer = Trainer(weights_summary='full',
                      progress_bar_refresh_rate=10,
                      num_sanity_val_steps=0,
                      gpus=0,
                      max_steps=120000,
                      logger=logger)

    trainer.fit(name_analyser, datamodule=datamodule)


if __name__ == '__main__':
    main()
