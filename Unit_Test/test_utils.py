from unittest import TestCase


class TestUtils(TestCase):
    def test_unicode_to_ascii(self):
        from NLP.utils.utils import unicode_to_ascii
        self.assertEqual(unicode_to_ascii('Ślusàrski'), "Slusarski")

    def test_read_lines(self):
        from NLP.utils.utils import read_lines
        self.assertEqual(read_lines(r"D:\PycharmProjects\nlp\Unit_Test\docs_test\Japanese.txt"),
                         ['Abe', 'Abukara', 'Adachi', 'Aida', 'Aihara'])
